# coding: utf-8

# Standard imports
import logging
import sys
import os
import pathlib

# External imports
import yaml
import wandb
import torch
import torchinfo.torchinfo as torchinfo
import tqdm
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from transformers import AlbertForSequenceClassification,RobertaForSequenceClassification

# Local imports
from . import data
from . import optim
from . import utils


def train(config):

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda") if use_cuda else torch.device("cpu")

    # Build the dataloaders
    logging.info("= Building the dataloaders")
    data_config = config["data"]

    train_loader, valid_loader, max_length = data.get_dataloaders(
        data_config, use_cuda
    )

    # Build the model
    logging.info("= Model")
    model_config = config["model"]
    if model_config["class"].lower() == "roberta":
        model = RobertaForSequenceClassification.from_pretrained("roberta-base", # Specify a pre-trained model
                                                                num_labels = 3,
                                                                output_attentions = False, # Output attention vectors
                                                                output_hidden_states = False, # Output hidden layers)
                                                                )
        if "fine_tuning" in model_config:
            model.load_state_dict(torch.load(model_config["fine_tuning"]), strict=False)
            for param in model.roberta.parameters():
                param.requires_grad = False

    elif model_config["class"].lower() == "albert":
        model = AlbertForSequenceClassification.from_pretrained("albert-base-v2", # Specify a pre-trained model
                                                                num_labels = 3,
                                                                output_attentions = False, # Output attention vectors
                                                                output_hidden_states = False, # Output hidden layers)
                                                                )
        if "fine_tuning" in model_config:
            model.load_state_dict(torch.load(model_config["fine_tuning"]), strict=False)
            for param in model.albert.parameters():
                param.requires_grad = False
    
    model.to(device)

    # Build the loss
    logging.info("= Training Loss")
    training_loss = optim.get_loss(config["training_loss"])

    # Build the optimizer
    logging.info("= Optimizer")
    optim_config = config["optim"]
    optimizer = optim.get_optimizer(optim_config, model.parameters())

    # Build the callbacks
    logging_config = config["logging"]
    # Let us use as base logname the class name of the modek
    logname = model_config["class"]
    logdir = utils.generate_unique_logpath(logging_config["logdir"], logname)
    if not os.path.isdir(logdir):
        os.makedirs(logdir)
    logging.info(f"Will be logging into {logdir}")

    # Copy the config file into the logdir
    logdir = pathlib.Path(logdir)
    with open(logdir / "config.yaml", "w") as file:
        yaml.dump(config, file)

    # Make a summary script of the experiment
    summary_text = (
        f"Logdir : {logdir}\n"
        + "## Command \n"
        + " ".join(sys.argv)
        + "\n\n"
        + f" Config : {config} \n\n"
        + "## Training Loss\n\n"
        + f"{training_loss}\n\n"
        + "## Datasets : \n"
        + f"Train : {train_loader.dataset}\n"
        + f"Validation : {valid_loader.dataset}"
    )
    with open(logdir / "summary.txt", "w") as f:
        f.write(summary_text)
    logging.info(summary_text)

    # Define the early stopping callback
    model_checkpoint = utils.ModelCheckpoint(
        model, str(logdir / "best_model.pt"), min_is_best=True
    )

    for e in range(config["nepochs"]):
        # Train 1 epoch
        train_loss = utils.train(model, train_loader, training_loss, optimizer, device)

        # Test
        test_loss = utils.test(model, valid_loader,training_loss, device)

        updated = model_checkpoint.update(test_loss)
        logging.info(
            "[%d/%d] Test loss : %.3f %s"
            % (
                e,
                config["nepochs"],
                test_loss,
                "[>> BETTER <<]" if updated else "",
            )
        )


def test(config):
    """
    Load le model torch.load(model_testing)
    Load le dataloader mais juste pour le testset -> recréer dataloader ou passer en arg que c'est le test.
    model.eval()
    model(donees)  = forward
    
    faire une fonction qui calcul le top 30 -> permet d'avoir une fct loss qu'on utilisera sur le validation set
    sortir un fichier avec le top 30
    """

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda") if use_cuda else torch.device("cpu")
    
    data_config = config["data"]
    test_loader = data.get_test_dataloader(data_config, use_cuda)

    model_config = config["model"]
    if model_config["class"].lower() == "roberta":
        model = RobertaForSequenceClassification.from_pretrained("roberta-base", # Specify a pre-trained model
                                                                num_labels = 3,
                                                                output_attentions = False, # Output attention vectors
                                                                output_hidden_states = False, # Output hidden layers)
                                                                )

        for param in model.roberta.parameters():
            param.requires_grad = False
    
    elif model_config["class"].lower() == "albert":
        model = AlbertForSequenceClassification.from_pretrained("albert-base-v2", # Specify a pre-trained model
                                                                num_labels = 3,
                                                                output_attentions = False, # Output attention vectors
                                                                output_hidden_states = False, # Output hidden layers)
                                                                )
        for param in model.albert.parameters():
            param.requires_grad = False
    model.load_state_dict(torch.load(model_config["path_to_test_model"]), strict=False)
    model.eval()
    model.to(device)

    prediction = []
    true_labels = []

    for batch in tqdm.tqdm(test_loader):
        b_input_ids = batch[0].to(device)
        b_input_mask = batch[1].to(device)
        b_labels = batch[2].to(device)

        with torch.no_grad():   
            # Get prediction by trained model
            preds = model(b_input_ids,
                        token_type_ids=None,
                        attention_mask=b_input_mask)
            prediction.append(preds[0].detach().cpu().numpy())
            true_labels.append(b_labels.detach().cpu().numpy())
    
    results = []
    for i in range(len(prediction)):
        for j in range(len(prediction[0])):
            results.append(prediction[i][j])

    # Predicted label list
    predicted_label = []
    for i in results:
        predicted_label.append(np.argmax(i, axis=0))

    pred_df = pd.DataFrame(predicted_label, columns=['pred_label'])

    # True label list
    true_labels2 = []
    for i in range(len(true_labels)):
        for j in range(data_config["batch_size"]):
            true_labels2.append(true_labels[i][j])

    label_df = pd.DataFrame(true_labels2, columns=['true_label'])

    y_pred = pred_df.values
    y_true = label_df.values

    print(classification_report(y_true, y_pred, digits=3))


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format="%(message)s")

    if len(sys.argv) != 3:
        logging.error(f"Usage : {sys.argv[0]} config.yaml <train|test>")
        sys.exit(-1)

    logging.info("Loading {}".format(sys.argv[1]))
    config = yaml.safe_load(open(sys.argv[1], "r"))

    command = sys.argv[2]
    eval(f"{command}(config)")
