# coding: utf-8

# Standard imports
import logging
import random
import os

# External imports
import torch
import torch.nn as nn
import torch.utils.data
import torchvision
from torchvision import transforms
from datasets import load_dataset
from torch.utils.data import TensorDataset
from transformers import AutoTokenizer



def get_dataloaders(data_config, use_cuda):
    batch_size = data_config["batch_size"]
    num_workers = data_config["num_workers"]

    logging.info("  - Dataset creation")
    base_dataset = load_dataset("snli")
    base_dataset = base_dataset.filter(lambda example: example['label'] != -1)
    logging.info(f"  - I loaded {len(base_dataset['train']['label'])} samples")

    tokenizer = AutoTokenizer.from_pretrained("cardiffnlp/twitter-roberta-base-emotion")
    max_length = get_max_length(base_dataset,tokenizer)



    # Let's try to get all features in train dataset.


    train_dataset = prep_dataset("train",base_dataset,tokenizer,max_length)

    valid_dataset = prep_dataset("validation",base_dataset,tokenizer,max_length)

    # Build the dataloaders
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=use_cuda,
    )

    valid_loader = torch.utils.data.DataLoader(
        valid_dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=num_workers,
        pin_memory=use_cuda,
    )

    return train_loader, valid_loader, max_length


def get_test_dataloader(data_config, use_cuda):
    batch_size = data_config["batch_size"]
    num_workers = data_config["num_workers"]

    logging.info("  - Dataset creation")

    base_dataset = load_dataset("snli")
    base_dataset = base_dataset.filter(lambda example: example['label'] != -1)
    logging.info(f"  - I loaded {len(base_dataset['train']['label'])} samples from test set")

    tokenizer = AutoTokenizer.from_pretrained("cardiffnlp/twitter-roberta-base-emotion")
    max_length = get_max_length(base_dataset,tokenizer)

    test_dataset = prep_dataset("test",base_dataset,tokenizer,max_length)

    # Build the dataloader
    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=num_workers,
        pin_memory=use_cuda,
    )


    return test_loader

def get_max_length(dataset,tokenizer):
    max_len = []
    train_s1 = dataset["train"]['premise']
    train_s2 = dataset["train"]['hypothesis']
    eval_s1 = dataset["validation"]['premise']
    eval_s2 = dataset["validation"]['hypothesis']
    test_s1 = dataset["test"]['premise']
    test_s2 = dataset["test"]['hypothesis']

    for sent1, sent2, sent3, sent4, sent5, sent6 in zip(train_s1, train_s2, eval_s1, eval_s2, test_s1, test_s2):
        token_words_1 = tokenizer.tokenize(sent1)
        token_words_2 = tokenizer.tokenize(sent2)
        token_words_3 = tokenizer.tokenize(sent3)
        token_words_4 = tokenizer.tokenize(sent4)
        token_words_5 = tokenizer.tokenize(sent5)
        token_words_6 = tokenizer.tokenize(sent6)

        token_words_1.extend(token_words_2)
        token_words_1.extend(token_words_3)
        token_words_1.extend(token_words_4)
        token_words_1.extend(token_words_5)
        token_words_1.extend(token_words_6)

        max_len.append(len(token_words_1))
    
    return max(max_len) + 1 # max length = Word tokens + 3 special tokens(1 [CLS] and 2 [SEP])

# Function to get word ID and attention mask
def prep_dataset(data_type,dataset,tokenizer,max_length):
  sent1,sent2,labels = dataset[data_type]["premise"],dataset[data_type]["hypothesis"],dataset[data_type]["label"]
  input_ids = []
  attention_masks = []
  #sentence_ids = []
  end_term = "<SEP>"

  for x , y in zip(sent1, sent2):
    sent= x + end_term + y
    
    encoded_dict = tokenizer.encode_plus(
        sent,
        # add_special_tokens = True, # Distinguish two sentences
        max_length = max_length, # Padding
        pad_to_max_length = True, # Padding
        return_attention_mask = True, # Make attention mask
        return_tensors = 'pt', # Return Pytorch tensors
        )
    
    # Get word ID
    input_ids.append(encoded_dict['input_ids'])
    
    # Get attention mask
    attention_masks.append(encoded_dict['attention_mask'])

    # Get token type ID (distinguish sentence 1 and 2)
    #sentence_ids.append(encoded_dict['token_type_ids'])
    
  # Concatenate listed tensor for vertical dimmention (dim=0)
  input_ids = torch.cat(input_ids, dim=0)
  attention_masks = torch.cat(attention_masks, dim=0)
  # Cast label list to tenosor
  labels = torch.tensor(labels)

  return TensorDataset(input_ids, attention_masks, labels)


if __name__ == "__main__":
    import yaml, sys
    config = yaml.safe_load(open(sys.argv[1], "r"))

    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda") if use_cuda else torch.device("cpu")
    
    # Build the dataloaders
    logging.info("= Building the dataloaders")
    data_config = config["data"]

    train_loader, valid_loader, input_sizes, num_classes = get_dataloaders(
        data_config, use_cuda
    )
