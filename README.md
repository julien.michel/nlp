# Testing our model

```
python -m torchtmpl.main config.yaml test
```

File _Roberta0.png_ is the result of our best model.
File best_model.pt contains the weigths of our best model.

# Description of our work

To begin the work, we tried to understand what the snli dataset was. It is a dictionnary dataset object composed of 3 datasets : _test_, _validation_ and _train_, all composed of 3 features _premise_, _hypothesis_ and _label_.

## Preprocessing

We tokenize our sentences and we search the maximum length of all the sentences. We then create a function _prep_ to concatenate our two sentences (_premise_ and _hypothesis_) with a 0-padding to the max length computed before. We return them into torch _TensorDataset_ in order to give them to our model.

## Train

We first tried the Albert model trained on all the layers, which gave us an accuracy of 0,89. 

We tried to increase the score by trying the Roberta model. 
First we trained all the layers giving us 0.9 accuracy. 
Then we tried to take a model from scratch and only train the last layer but it only gave us a disappointing 0.6 accuracy. 
So we took the already computed weights of all but the last layers and only train the last layer. We ended up with a 0.9 accuracy as the training of all the layers.

We took the Roberta trained on all layers as our best model (cf. Roberta0.png and best_model.pt for result and weigths).
